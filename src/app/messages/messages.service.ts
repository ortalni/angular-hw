import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class MessagesService {
  http:Http; //http is the name, Http is the type
  getMessages(){
   //return ['Message1', 'Message2', 'Message3', 'message4'];
   //get messages from the SLIM rest API (dont say DB)
   return this.http.get('http://localhost/messages'); 

  }
  getMessage(id){
   return this.http.get('http://localhost/messages/'+ id); 

  }

  postMessage(data){
    let options = {
      headers:new Headers({
        'content-type':'application/json;charset=utf-8'
      })
    }
    
    return this.http.post('http://localhost/messages', data, options);
    
  }
// תרגיל 6 עדכון לאחר לחיצה על הכפתור. שליחה כג'ייסון
  putMessage(data,key){
    let options = {
      headers:new Headers({
        'content-type':'application/json;charset=utf-8'
      })
    }
    
    return this.http.put('http://localhost/messages/'+key, data, options);
    
  }

  deleteMessage(key){
    return this.http.delete('http://localhost/messages/'+key);
  }

  constructor(http:Http) { //http is a new object of type Http
    this.http = http;
  }

}