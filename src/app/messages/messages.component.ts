import { MessagesService } from './messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {


  //messages = ['Message1', 'Message2', 'Message3', 'message4'];
  messages;
  messagesKeys = [];

  constructor(private service:MessagesService) {
    //let service = new MessagesService();
    service.getMessages().subscribe(response => {
      //console.log(response.json());
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);
    });
  }

  optimisticAdd(message){
    //console.log("addMessage worked" + message);
    var newKey = this.messagesKeys[this.messagesKeys.length-1] + 1;
    var newMessageObject = {};
    newMessageObject['body'] = message;
    this.messages[newKey] = newMessageObject;
    this.messagesKeys = Object.keys(this.messages);
  }

  pessimisticAdd(){
    this.service.getMessages().subscribe(response => {
    //console.log(response.json());
    this.messages = response.json();
    this.messagesKeys = Object.keys(this.messages);
  })}


  deleteMessage(key){
    console.log(key);
    let index = this.messagesKeys.indexOf(key);
    this.messagesKeys.splice(index,1);
    //delete from server
    this.service.deleteMessage(key).subscribe(
      response=>console.log(response)
    );
  }

  ngOnInit() {
  }

}