import { MessagesService } from './../messages.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {ActivatedRoute, Router} from'@angular/router';

@Component({
  selector: 'messagesForm',
  templateUrl: './messages-form.component.html',
  styleUrls: ['./messages-form.component.css']
})
export class MessagesFormComponent implements OnInit {
// תרגיל 6
   currentMessage;
   submitButtonText="Send message";
//
    msgform = new FormGroup({
    message:new FormControl(),
    user_id:new FormControl(),
  });

  constructor(private route:ActivatedRoute, private service:MessagesService,        
    private router: Router,
  ) {}
  ngOnInit() {
    //תרגיל 6
      this.route.paramMap.subscribe(params=>{
        let id = params.get('id');
        if (id) {
          this.submitButtonText="Update";
          this.service.getMessage(id).subscribe(response=>{
            this.currentMessage = response.json();
            this.msgform.controls.user_id.setValue(this.currentMessage.user_id);
            this.msgform.controls.message.setValue(this.currentMessage.body);         
         });
        }
    });
    //
  }
  

  @Output() addMessage:EventEmitter<any> = new EventEmitter<any>(); //any is the type. could be [] or {}
  @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>(); //pessimistic


  sendData(){
    console.log(this.msgform.value);
    //תרגיל 6 Update
    if(this.currentMessage) {
      this.service.putMessage(this.msgform.value, this.currentMessage.id).subscribe(
        response => {
         this.router.navigateByUrl("/");
          
        },
      error => {
        console.log(error.json());

        }  );
        // Save
    } else {
    this.service.postMessage(this.msgform.value).subscribe(
      response => {
        this.addMessage.emit(this.msgform.value.message);        
        console.log(response.json());
        this.addMessagePs.emit();
      },
      error => {
        console.log(error.json());
        
      } )
  }
    
  }

  


}
