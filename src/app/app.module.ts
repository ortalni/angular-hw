import {RouterModule} from '@angular/router';
import { HttpModule } from "@angular/http";
import { MessagesService } from './messages/messages.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UsersService } from './users/users.service';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { UsersComponent } from './users/users.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import{ FormsModule,ReactiveFormsModule} from "@angular/forms";
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';





@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent,
    MessagesFormComponent,
    NotFoundComponent,    
    NavigationComponent,
    MessageComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'',component:MessagesComponent},
      {path:'users',component:UsersComponent},
      {path:'message/:id', component:MessageComponent},
      //תרגיל 6
      {path:'messageForm/:id', component:MessagesFormComponent},      
      {path:'**',component:NotFoundComponent},
    ])
  ],
  providers: [
  MessagesService,
  UsersService
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
