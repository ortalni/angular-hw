import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class UsersService {
  http:Http;
  getMessages(){
    //return ['Message1','Message2','Message3','Message4'];
    //get users from the SLIM rest API (Don't say DB)
   return this.http.get('http://localhost/users');

  }
// http- שם, Http- סוג
// האובייקט נוצר בסוגריים
  constructor(http:Http) {
    this.http = http;

   }
 }