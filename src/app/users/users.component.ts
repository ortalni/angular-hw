import { UsersService } from './users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
users;
usersKeys;

  // מייצר אובייקט ומזריק ל component
  constructor(service:UsersService) { 
    service.getMessages().subscribe(response=>{
      //console.log(response.json());
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    });

  }

  ngOnInit() {
  }

}
